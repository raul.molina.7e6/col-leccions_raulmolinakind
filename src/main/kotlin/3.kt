import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val delegats = mutableSetOf<String>()
    val votacions = mutableMapOf<String,Int>()
    var vot = scanner.next()
    while (vot.uppercase() != "END"){
        if(delegats.add(vot)) {
            delegats.add(vot)
            votacions.put(vot, 1)
        }else{
            votacions[vot] = votacions[vot]!! + 1
        }
        vot = scanner.next()
    }
    println(votacions)
}
