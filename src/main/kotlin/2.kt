import java.util.Scanner

data class Employe(
    val nom: String,
    val cognom: String,
    val dni: String,
    val adreça: String
    )

fun main() {
    val scanner = Scanner(System.`in`)
    val empleats = mutableMapOf<String,Employe>()
    val numOfEntries = scanner.nextInt()
    for(i in 0 until numOfEntries){
        println("DNI")
        val dni = scanner.next()
        println("Nom")
        val nom = scanner.next()
        println("Cognom")
        val cognom = scanner.next()
        println("Adreça")
        val adreça = scanner.nextLine()
        empleats.put(dni,Employe(nom,cognom,dni,adreça))
    }

    while (true){
        val entrada = scanner.next()
        if(entrada.uppercase() == "END") break
        println("${empleats[entrada]?.nom} ${empleats[entrada]?.cognom} - ${empleats[entrada]?.dni}, ${empleats[entrada]?.adreça}")
    }
}
