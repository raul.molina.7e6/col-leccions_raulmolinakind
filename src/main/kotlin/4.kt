import java.util.Scanner

fun main() {
    val scanner = Scanner(System.`in`)
    val wordList = mutableListOf<String>()
    var word = scanner.next()
    while (word.uppercase() != "END"){
        if(word !in wordList)wordList.add(word)
        else println("MEEEC!")
        word = scanner.next()
    }
}
