import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val signs = mutableMapOf<Int,String>()
    val numOfSigns = scanner.nextInt()
    for(i in 0 until numOfSigns){
        val metro = scanner.nextInt()
        val nameSign = scanner.next()
        signs.put(metro, nameSign)
    }
    while (true){
        val entrada = scanner.nextInt()
        if(entrada in signs){
            println(signs[entrada])
        }else{
            println("no hi ha cartell")
        }
    }
}
